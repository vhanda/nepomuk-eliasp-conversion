#include <QtCore/QCoreApplication>
#include <QtCore/QTimer>

#include <Soprano/Statement>
#include <Soprano/Node>
#include <Soprano/Model>
#include <Soprano/QueryResultIterator>
#include <Soprano/StatementIterator>
#include <Soprano/NodeIterator>
#include <Soprano/PluginManager>

#include <Nepomuk/Resource>
#include <Nepomuk/ResourceManager>
#include <Nepomuk/Variant>
#include <Nepomuk/File>
#include <Nepomuk/Tag>

#include <Nepomuk/Types/Property>
#include <Nepomuk/Types/Class>

#include <Nepomuk/Query/Query>
#include <Nepomuk/Query/FileQuery>
#include <Nepomuk/Query/ComparisonTerm>
#include <Nepomuk/Query/LiteralTerm>
#include <Nepomuk/Query/ResourceTerm>
#include <Nepomuk/Query/QueryServiceClient>
#include <Nepomuk/Query/ResourceTypeTerm>
#include <Nepomuk/Query/QueryParser>
#include <Nepomuk/Query/Result>

#include <KDebug>

// Vocabularies
#include <Soprano/Vocabulary/RDF>
#include <Soprano/Vocabulary/RDFS>
#include <Soprano/Vocabulary/NRL>
#include <Soprano/Vocabulary/NAO>

#include <Nepomuk/Vocabulary/NIE>
#include <Nepomuk/Vocabulary/NFO>
#include <Nepomuk/Vocabulary/NMM>
#include <Nepomuk/Vocabulary/NCO>
#include <Nepomuk/Vocabulary/PIMO>

#include <KUrl>
#include <KJob>
#include <KCmdLineArgs>
#include <KAboutData>

#include <nepomuk/datamanagement.h>
#include <nepomuk/simpleresource.h>
#include <nepomuk/simpleresourcegraph.h>

#include <iostream>

using namespace Soprano::Vocabulary;
using namespace Nepomuk::Vocabulary;

class TestObject : public QObject {
    Q_OBJECT
public slots:
    void main();

public:
    TestObject() {
        QTimer::singleShot( 0, this, SLOT(main()) );
    }
};

int main( int argc, char ** argv ) {
    KAboutData aboutData("newepomukeliasp", 0, ki18n("newepomukeliasp"),
                         "1.0",
                         ki18n("A Nepomuk migrater written for Eliasp"),
                         KAboutData::License_LGPL_V2,
                         ki18n("(C) 2011, Vishesh Handa"));
    aboutData.addAuthor(ki18n("Vishesh Handa"), ki18n("Current maintainer"), "handa.vish@gmail.com");

    KCmdLineArgs::init(argc, argv, &aboutData);

    KCmdLineOptions options;
    options.add("+url", ki18n("The base directory which contains all the files"));

    KCmdLineArgs::addCmdLineOptions(options);
    const KCmdLineArgs *args = KCmdLineArgs::parsedArgs();

    // Application
    QCoreApplication app( argc, argv );
    KComponentData data( aboutData, KComponentData::RegisterAsMainComponent );

    if( args->count() != 1 ) {
        std::cout << "Error! Incorrect arguments" << std::endl;
    }

    TestObject a;
    app.exec();
}

void TestObject::main()
{
    const KCmdLineArgs *args = KCmdLineArgs::parsedArgs();
    KUrl baseDir( args->url(0) );
    baseDir.setScheme("file");

    const QString query = QString::fromLatin1("select distinct ?r ?url where { "
                                              "?r %1 ?url . "
                                              "FILTER(regex(str(?url), '^%2')). }")
                          .arg( Soprano::Node::resourceToN3( NIE::url() ),
                                baseDir.url( KUrl::AddTrailingSlash ) );

    kDebug() << "Checking for resources in " << baseDir.url( KUrl::AddTrailingSlash );

    Soprano::Model *model = Nepomuk::ResourceManager::instance()->mainModel();
    Soprano::QueryResultIterator it = model->executeQuery( query, Soprano::Query::QueryLanguageSparql );

    QHash<QUrl, QUrl> resources;
    while( it.next() ) {
        kDebug() << "Found " << it[0].toN3() << " --> " << it[1].uri();
        resources.insert( it[0].uri(), it[1].uri() );
    }
    kDebug() << "Total: " << resources.size();

    QHashIterator<QUrl, QUrl> iter( resources );
    while( iter.hasNext() ) {
        iter.next();

        Soprano::Statement st( iter.key(), NIE::url(), iter.value() );
        QList<Soprano::Node> l = model->listStatements( st ).iterateContexts().allNodes();
        const QUrl graph = l.isEmpty() ? QUrl() : l.first().uri();
        if( graph.isEmpty() ) {
            kDebug() << "Warning --- FOUND AN EMPTY GRAPH -- " << st;
        }

        st.setContext( graph );

        kDebug() << "Converting " << iter.value();
        // Remove the statement and add it back
        // The internal RemoveableMediaModel should take of handling the nie:url properly.
        model->removeAllStatements( st );
        model->addStatement( st );
    }

    QCoreApplication::instance()->quit();
}

#include "main.moc"
